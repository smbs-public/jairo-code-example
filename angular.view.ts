<div
 (click)="onToggles()"
 class="overlay-pullup"
 [hidden]="collapsed === 0"
></div>
 
<lib-ionic-pullup
 (expanded)="onExpand()"
 (collapsed)="onCollapse()"
 [(state)]="collapsed"
 [toolbarTopMargin]="128"
 [minBottomVisible]="-150"
>
  <div class="thumb-pullup" [hidden]="!collapsed">
    <ion-img
     class="fit-cover"
     [src]="avatar"
     alt=""
     (ionError)="$event.target.src = BANNER"
   ></ion-img>
  </div>
 
  <ion-content class="container-pullup">
    <ion-variant-header
     [name]="product?.name || ''"
     (onToggle)="onToggles()"
   ></ion-variant-header>
 
    <ion-list *ngIf="variants?.length; else empty" class="ion-margin-start">
      <ion-item lines="none">
        <ion-row style="width: 100%; margin-left: 60px">
          <ion-col
           class="ion-align-self-center"
           [size]="head.size"
           *ngFor="let head of headers"
         >
            <p class="ion-text-center ion-text-nowrap text-first text-xs fw700">
              {{ head.title }}
            </p>
          </ion-col>
        </ion-row>
      </ion-item>
 
      <ng-container *ngFor="let variant of variants; let index = index">
        <ion-variant-item
         [variant]="variant"
         [prop]="'count'"
         [readOnly]="false"
         (onChange)="onChangeVariant($event)"
       ></ion-variant-item>
      </ng-container>
    </ion-list>
  </ion-content>
 
  <ion-footer class="pullup">
    <ion-button
     class="ion-no-margin left"
     size="large"
     (click)="onFavorite($event)"
   >
      <h3><i class="zmdi zmdi-favorite text-size-lg text-primary"></i></h3>
    </ion-button>
    <ion-button
     class="ion-no-margin right"
     size="large"
     (click)="onAdd($event)"
   >
      <i class="zmdi zmdi-check text-size-lg text-white ion-margin-end"></i>
      {{ label }}
    </ion-button>
  </ion-footer>
</lib-ionic-pullup>
 
<ng-template #empty>
  <ion-text class="text-third">
    <p class="ion-padding ion-margin text-light ion-text-center">
      {{ "No hay variantes registradas..." }}
    </p>
  </ion-text>
</ng-template>
