import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Variant } from "../../../models/Variant";
import { IonPullUpFooterState } from "ionic-pullup";
import { Utils } from "../../../utils/Utils";
import { Gallery } from "../../../utils/Gallery";
import { ModalController } from "@ionic/angular";
import { BANNER_PLACEHOLDER, TOAST_BOTTOM } from "../../../utils/Constants";
import { VariantService } from "../../../services/variant/variant.service";
import { AlertsService } from "../../../services/alerts/alerts.service";
import { UploadService } from "../../../services/upload/upload.service";
import { Spinners } from "../../../utils/Spinners";
import { CartStore } from "../../../store/cart.store";
import { ProductStore } from "../../../store/product.store";
import { Client } from "../../../models/Client";
 
@Component({
  selector: "ion-variant-list",
  templateUrl: "./ion-variant-list.component.html",
  styleUrls: ["./ion-variant-list.component.scss"],
})
export class IonVariantListComponent implements OnInit, OnChanges, OnDestroy {
  @Input("client") client?: Client;
  @Input("collapsed") collapsed: IonPullUpFooterState;
  @Input("spinner") spinner? = new Spinners();
  @Output("onSet") onSet?: EventEmitter<any> = new EventEmitter<any>();
  @Output("onSubmit") onSubmit?: EventEmitter<any> = new EventEmitter<any>();
  @Output("onToggle") onToggle?: EventEmitter<any> = new EventEmitter<any>();
  @Output("onChange") onChange?: EventEmitter<any> = new EventEmitter<any>();
 
  @ViewChild("content") content;
  @ViewChild("footer") footer;
 
  form: FormGroup = null;
  variant: Variant = null;
  product: any = null;
  cart: any = null;
  modal = null;
  BANNER = BANNER_PLACEHOLDER;
 
  util: Utils = new Utils();
  gallery: Gallery = new Gallery();
  headers: any[] = [
    { title: "Variante", size: 4 },
    { title: "Precio", size: 2 },
    { title: "P/ Crédito", size: 2 },
    { title: "Cantidad", size: 4 },
  ];
 
  cartSubscription: any = null;
  productSubscription: any = null;
 
  constructor(
    public modalCtrl: ModalController,
    private variantService: VariantService,
    private alertsService: AlertsService,
    private uploadService: UploadService,
    private cartStore: CartStore,
    private productStore: ProductStore
  ) {
    this.cartSubscription = null;
    this.productSubscription = null;
  }
 
  get variants(): Array<Variant> {
    return this.product?.variants || [];
  }
 
  get avatar() {
    if (this.product?.variants?.length) {
      const [first, ...rest] = this.product?.variants;
      return first?.image !== ""
        ? first?.image
        : first?.file?.base64 || BANNER_PLACEHOLDER;
    } else {
      const images: string[] =
        this.product?.images !== "" ? this.product?.images?.split(",") : [];
      if (images?.length) return images[0];
    }
 
    return BANNER_PLACEHOLDER;
  }
 
  get label() {
    const exists = this.variants.some((v) => v.count > 0);
    return exists ? "Actualizar la cesta" : "Añadir a la cesta";
  }
 
  ngOnInit(): void {
    /**
     * SUBSCRIBING TO PRODUCT STORE
     * */
    this.productSubscription = this.productStore
      .store()
      .subscribe((state: any) => {
        this.product = state?.product;
      });
 
    /**
     * SUBSCRIBING TO CART STORE
     * */
    this.cartSubscription = this.cartStore.store().subscribe((state: any) => {
      Object.values(state?.carts || []).forEach((cart: any) => {
        if (this.client?.id === cart?.client?.id) {
          this.cart = cart;
 
          if (this.product?.id && this.cart?.data) {
            const data = this.cart?.data[this.product?.id] || null;
 
            if (data)
              this.product?.variants?.forEach((variant: Variant) => {
                variant.count = 0;
                const _variants = data?.variants || [];
 
                _variants?.forEach((_v) => {
                  if (_v.id === variant.id) variant.count = _v.count;
                });
              });
          }
        }
      });
    });
  }
 
  ngOnChanges(changes: SimpleChanges): void {
    this.cartStore.init();
  }
 
  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
    this.productSubscription.unsubscribe();
  }
 
  onChangeVariant($variant: Variant) {
    this.product.variants.forEach((v) =>
      v.id == $variant.id ? { ...v, count: $variant.count } : { ...v }
    );
    this.productStore.reAssign($variant);
  }
 
  onAdd($event) {
    this.cart = {
      ...this.cart,
      client: {
        id: this.client.id,
        name: this.client.name,
        avatar: this.client.avatar,
      },
      data: {
        ...this.cart?.data,
        [this.product?.id]: {
          product: {
            id: this.product?.id,
            name: this.product?.name,
          },
          variants: this.product?.variants
            .filter(({ count }) => count > 0)
            .map(({ id, image, count, name, price, price_credit, status, cost, stock }: Variant) => {
                return {
                  id: id,
                  code: Utils.getId(),
                  productId: this.product?.id,
                  status: status,
                  image: image,
                  count: count,
                  name: name,
                  price: price,
                  price_credit: price_credit,
                  sales: price,
                  cost: cost,
                  stock: stock,
                };
              }
            ),
        },
      },
    };
 
    this.cartStore.reAssign(this.cart);
 
    this.alertsService
      .presentToast("¡Agregado!", 3000, TOAST_BOTTOM)
      .then(() => this.onToggle.emit());
  }
 
  onFavorite($event) {}
 
  onToggles() {
    this.onToggle.emit();
  }
 
  onExpand() {
    this.collapsed = IonPullUpFooterState.Expanded;
  }
 
  onCollapse() {
    this.collapsed = IonPullUpFooterState.Collapsed;
  }
 
  onReset() {
    this.variant = null;
    if (this.modal) this.modal.dismiss();
  }
}
